module raptor {
	requires dino;

	provides com.ryaneorth.dino.Dinosaur with com.ryaneorth.raptor.Raptor;
}