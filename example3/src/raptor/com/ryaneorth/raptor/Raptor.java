package com.ryaneorth.raptor;

import com.ryaneorth.dino.Dinosaur;

public class Raptor implements Dinosaur {
	private static final String NAME = "Rodger the Raptor";

	public String getName() {
		return NAME;
	}
}
