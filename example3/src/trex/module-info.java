module trex {
	requires dino;

	provides com.ryaneorth.dino.Dinosaur with com.ryaneorth.trex.TRex;
}