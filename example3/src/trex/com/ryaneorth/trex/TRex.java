package com.ryaneorth.trex;

import com.ryaneorth.dino.Dinosaur;

public class TRex implements Dinosaur {
	private static final String NAME = "Tony the T-Rex";

	public String getName() {
		return NAME;
	}
}
