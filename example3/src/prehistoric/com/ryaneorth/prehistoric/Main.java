package com.ryaneorth.prehistoric;

import java.lang.Iterable;
import java.util.Arrays;
import java.util.List;
import java.util.ServiceLoader;

import com.ryaneorth.dino.Dinosaur;

public class Main {
	public static void main(String[] args) {
		Iterable<Dinosaur> dinos = ServiceLoader.load(Dinosaur.class);

		for(Dinosaur dino : dinos) {
			System.out.println("Hi! My name is " + dino.getName());
		}
	}
}
