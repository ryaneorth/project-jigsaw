#!/bin/bash

if [ -d "mods" ]; then
	rm -r mods
fi

if [ -d "mlib" ]; then
	rm -r mlib
fi

if [ -d "executables" ]; then
	rm -rf executables
fi
