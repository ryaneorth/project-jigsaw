package com.ryaneorth.prehistoric;

import java.util.Arrays;
import java.util.List;
import com.ryaneorth.dino.Dinosaur;
import com.ryaneorth.raptor.Raptor;
import com.ryaneorth.trex.TRex;

public class Main {
	public static void main(String[] args) {
		List<Dinosaur> dinos = Arrays.asList((Dinosaur) new Raptor(), (Dinosaur) new TRex());

		dinos.forEach(dino -> System.out.println("Hi! My name is " + dino.getName()));
	}
}
