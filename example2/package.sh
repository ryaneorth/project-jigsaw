#!/bin/bash

./build.sh

mkdir mlib

jar --create --file=mlib/dino.jar -C mods/dino .
jar --create --file=mlib/raptor.jar -C mods/raptor .
jar --create --file=mlib/trex.jar -C mods/trex .

jar --create --file=mlib/prehistoric.jar --main-class=com.ryaneorth.prehistoric.Main -C mods/prehistoric .
