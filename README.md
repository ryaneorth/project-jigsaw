# Java 9: Project Jigsaw

Java 9 had a lot of new features come with it, but none are more intriguing than Oracle's solution for native modularity within the JVM: Project Jigsaw.

## Project Jigsaw Explained

### What is it?
Project Jigsaw is a new feature in Java 9 allowing developers to break up their applications into stand-alone modules. Under development for nearly 10 years prior to its release, it is considered one of the more disruptive changes to the JDK since Java was first introduced.

### What is a module?
A module is just a .jar file with a special file named `module-info.java` at its root. This file serves two main purposes:

* States what other modules it is dependent on
* States what packages withhin the module it exports for other modules to consume

Here is a sample `module-info.java`:

```
module com.ryaneorth.module1 {
	exports com.ryaneorth.module1.mypackage;
	requires com.ryaneorth.module2;
	requires com.ryaneorth.module3;
}
```

Let's breakdown what is going on here:

* The module name is `com.ryaneorth.module1`. Any other modules that depend on this module will reference it using this name in their `module-info.java`.
* It exports the package `com.ryaneorth.module1.mypackage`. This module may have other packages in it, but only this package can be consumed by other modules. This allows developers to hide packages that are not meant to be used outside of the module.
* Finally, the module definition states that it has requirements on `com.ryaneorth.module2` and `com.ryaneorth.module3`. If these modules are not included on the *module path* during compilation, an error will be thrown.

Wait... what is the *module path*? The module path is similar to the classpath - but instead of defining individual classes/jars, the location of modules needed for compilation are supplied. When compiling (via `javac`) the module path is set using the `--module-source-path` command followed by the location of any modules we want to include on the module path.

A couple of other things to note before we move on:

* Cyclic dependencies are not allowed (e.g. module1 cannot require module2 and module2 require module1). This is enforced at compile time.
* All modules have an implicit dependency on a module named `java.base`. We'll discuss this more in the next section.
* Class.forName(...) and reflection cannot be used to access un-exported packages. Packages that are not explicitly exported by their module are hidden from the outside world.
* Any .jar file that does not have a `module-info.java` (e.g. any .jar file created prior to the release of Java 1.9) exports all of their packages by default.

### The JDK itself is now modular!
To understand this, lets first look at what the JDK looked like prior to Java 1.9:

![Pre-Java9](images/preJava9.png)

Here you can see that there is a file named `rt.jar` which is ~70 MB in size. This single file contains the majority of the classes you use every day when developing in Java: java.lang, java.util, etc. Compare this to the Java 9 JDK:

![Java9](images/java9.png)

Here you can see number of files with a .jmod extension. A \*.jmod file is simply a module with a special extension Oracle has given it to indicate it is part of the JDK. In other words, these are just .jar files with a different extension. What Oracle did here was break up their own `rt.jar` file into smaller, modularized pieces.

You'll notice one of the .jmod files in the above image is much larger in size than the others: `java.base.jmod`. This module contains what Oracle considers the *essential* packages for building a Java application. As a result, every other module has an implicit dependency on this module (e.g. it does not need to be explicitly declared in `module-info.java`).

## Code Examples

Before running these examples make sure you have JDK 9 downloaded from the [Oracle](https://www.oracle.com/java/java9.html) website and that it is on your system `PATH`.

### Example 1
In this repository you will find 3 examples. The first example, `example1`, starts simple. It has 2 modules: `prehistoric` and `dino`, each with only 1 class. Here is what the dependency graph looks like:

![Example1](images/example1.png)

The prehistoric module has a simple main class (`Main.java`) which creates an instance of the `Dinosaur` class and calls its `getName` method. 

```
public class Main {
	public static void main(String[] args) {
		Dinosaur dino = new Dinosaur();
		System.out.println("Hi! My name is " + dino.getName());
	}
}

```

```
public class Dinosaur {
	private static final String NAME = "Donny the dino";

	public String getName() {
		return NAME;
	}
}
```

If you execute the `run.sh` script in the `example1` directory you can compile and execute the code to see the result. You can also execute the `finddeps.sh` script which lists the dependencies of each module.


### Example 2
Example 2 gets a little more complicated. In this example there are now 4 modules:

![Example2](images/example2.png)

Again, each module only has 1 class. Here is what things look like now:

Main.java (in the `prehistoric` module):

```
public class Main {
	public static void main(String[] args) {
		List<Dinosaur> dinos = Arrays.asList((Dinosaur) new Raptor(), (Dinosaur) new TRex());

		dinos.forEach(dino -> System.out.println("Hi! My name is " + dino.getName()));
	}
}

```

Dinosaur.java (in the `dino` module):

```
public interface Dinosaur {
	String getName();
}

```


TRex.java (in the `trex` module):

```
public class TRex implements Dinosaur {
	private static final String NAME = "Tony the T-Rex";

	public String getName() {
		return NAME;
	}
}

```

Raptor.java (in the `raptor` module):

```
public class Raptor implements Dinosaur {
	private static final String NAME = "Rodger the Raptor";

	public String getName() {
		return NAME;
	}
}
```

If you run the `run.sh` script in the example2 directory you will see the following output:

```
Hi! My name is Tony the T-Rex
Hi! My name is Rodger the Raptor

```

This is pretty much what you'd expect looking at the above code, but there is something troubling if you look a the dependency graph for this example. There is an incredibly high level of coupling between the modules. Specifically, the `prehistoric` module has explicit dependencies on all other modules. Let's jump to example 3 and see how we can solve this.


### Example 3
If you were to execute the `run.sh` script right away in the `example3` directory you would see the same output as in example 2. While this may not see like a big difference, lets take a look at the dependency graph:

![Example3](images/example3.png)

The code itself has not changed from example 2 (at least not in any significant fashion). What has changed, however, is the way the modules are defined. Notice the two new keywords in the above image:

* *provides* indicates that the module has an implementation of the specified interface
* *uses* indicates that the module uses any implementations of the specified interface on the *module path*

The only code that changed is in the `Main.java` class in the *prehistoric* module:

```
public class Main {
	public static void main(String[] args) {
		Iterable<Dinosaur> dinos = ServiceLoader.load(Dinosaur.class);

		for(Dinosaur dino : dinos) {
			System.out.println("Hi! My name is " + dino.getName());
		}
	}
}
```

As you can see in comparing this version of the `Main.java` class to the one in the previous example, we are dynamically loading the implementations of the `Dinosaur` interface at runtime using the `java.util.ServiceLoader` class. This is great for a few reasons:

* The *dino* module no longer needs to have explicit dependencies on the *raptor* or *trex* modules.
* If at runtime we wanted to include a new module that has a class which implements the `Dinosaur` interface we could do so and the `java.util.ServiceLoader` would pick it up when the code is run.

You can test this yourself by creating your own module that has an implementation of the `Dinosaur` interface and including it on the *module path* at runtime.


## Conclusion
Whether this new modular architecture will find its way into they typical workflow of Java application developers in the future is yet to be seen. That being said, with the above examples it is easy to see the potential of Project Jigsaw.  For more information on this topic, feel free to check out the below links:

* [http://www.baeldung.com/project-jigsaw-java-modularity](http://www.baeldung.com/project-jigsaw-java-modularity)
* [https://dzone.com/articles/the-features-project-jigsaw-brings-to-java-9-1](https://dzone.com/articles/the-features-project-jigsaw-brings-to-java-9-1)
* [https://mreinhold.org/blog/jigsaw-complete](https://mreinhold.org/blog/jigsaw-complete)
* [https://www.javaworld.com/article/2878952/java-platform/modularity-in-java-9.html](https://www.javaworld.com/article/2878952/java-platform/modularity-in-java-9.html)

