package com.ryaneorth.prehistoric;

import com.ryaneorth.dino.Dinosaur;

public class Main {
	public static void main(String[] args) {
		Dinosaur dino = new Dinosaur();
		System.out.println("Hi! My name is " + dino.getName());
	}
}
